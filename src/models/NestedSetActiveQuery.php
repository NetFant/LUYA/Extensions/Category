<?php
namespace netfant\category\models;

use creocoder\nestedsets\NestedSetsQueryBehavior;
use luya\admin\ngrest\base\NgRestActiveQuery;

/**
 * NestedSetActiveQuery
 *
 * This class is used to adjust the default \luya\admin\ngrest\base\NgRestActiveQuery
 *
 * It should be used by the model's find() method.
 *
 *  ```public static function find()
 *  {
 *      return new NestedSetActiveQuery(get_called_class());
 *  }```
 *
 * @author    Alexander Schmid <schmid@netfant.ch>
 * @copyright 2019 NetFant Schmid
 * @version   1.0.0
 * @since     1.0.0
 */
class NestedSetActiveQuery extends NgRestActiveQuery
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::class
        ];
    }
}