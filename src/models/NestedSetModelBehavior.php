<?php
namespace netfant\category\models;

use creocoder\nestedsets\NestedSetsBehavior;

/**
 * NestedSetModelBehavior
 *
 * The behavior should be added to the model.
 *
 * ```public function behaviors()
 * {
 *     return array_merge(parent::behaviors(), [
 *         'tree' => [
 *             'class' => NestedSetModelBehavior::class
 *         ]
 *     ]);
 * }```
 *
 *  Model transactions must be set.
 *
 *  ```public function transactions()
 *  {
 *      return [
 *          self::SCENARIO_DEFAULT => self::OP_ALL,
 *      ];
 *  }```
 *
 * Adjust the find() method to use the NestedSetsActiveQuery
 *
 *  ```public static function find()
 *  {
 *      return new NestedSetActiveQuery(get_called_class());
 *  }```
 *
 * @author    Alexander Schmid <schmid@netfant.ch>
 * @copyright 2019 NetFant Schmid
 * @version   1.0.0
 * @since     1.0.0
 */
class NestedSetModelBehavior extends NestedSetsBehavior
{
    public $operation;
    public $operationItem;
}