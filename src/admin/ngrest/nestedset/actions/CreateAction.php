<?php

namespace netfant\category\admin\ngrest\nestedset\actions;

use creocoder\nestedsets\NestedSetsBehavior;
use luya\Exception;
use netfant\category\models\NestedSetModelBehavior;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;

/**
 * Create action
 *
 * This class is used for the nested set crud.
 *
 * @author    Alexander Schmid <schmid@netfant.ch>
 * @copyright 2019 NetFant Schmid
 * @version   1.0.0
 * @since     1.0.0
 */
class CreateAction extends \luya\admin\ngrest\base\actions\CreateAction
{
    private function successResponse($model)
    {
        $response = Yii::$app->getResponse();
        $response->setStatusCode(201);
        $id = implode(',', array_values($model->getPrimaryKey(true)));
        $response->getHeaders()->set('Location', Url::toRoute([$this->viewAction, 'id' => $id], true));
    }

    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /* @var $model \yii\db\ActiveRecord */
        $model = new $this->modelClass([
            'scenario' => $this->scenario,
        ]);

        $params = Yii::$app->getRequest()->getBodyParams();

        if (!empty($params['operation'])) {
            $operation = $params['operation'];
            unset($params['operation']);
        }
        if (!empty($params['operationItem'])) {
            $operationItem = $params['operationItem'];
            unset($params['operationItem']);
        }

        $model->load($params, '');

        if ($operation == NestedSetModelBehavior::OPERATION_MAKE_ROOT) {
            if ($model->makeRoot()) {
                $this->successResponse($model);
            } else {
                throw new ServerErrorHttpException("Make root operation did not succeed");
            }
        } else {
            if (!empty($item = $this->modelClass::findOne($operationItem))) {
                switch ($operation) {
                    case NestedSetModelBehavior::OPERATION_PREPEND_TO:
                        if ($model->prependTo($item)) {
                            $this->successResponse($model);
                        } else {
                            throw new ServerErrorHttpException("Prepend to operation did not succeed");
                        }
                        break;
                    case NestedSetModelBehavior::OPERATION_APPEND_TO:
                        if ($model->prependTo($item)) {
                            $this->successResponse($model);
                        } else {
                            throw new ServerErrorHttpException("Append to operation did not succeed");
                        }
                        break;
                    case NestedSetModelBehavior::OPERATION_INSERT_BEFORE:
                        if ($model->insertBefore($item)) {
                            $this->successResponse($model);
                        } else {
                            throw new ServerErrorHttpException("Insert before operation did not succeed");
                        }
                        break;
                    case NestedSetModelBehavior::OPERATION_INSERT_AFTER:
                        if ($model->insertAfter($item)) {
                            $this->successResponse($model);
                        } else {
                            throw new ServerErrorHttpException("Insert after operation did not succeed");
                        }
                        break;
                    default:
                        throw new ServerErrorHttpException("Operation did not work: Operation not found");
                        break;
                }
            } else {
                throw new ServerErrorHttpException("Operation did not work: item not found");
            }
        }

        return $model;
    }
}