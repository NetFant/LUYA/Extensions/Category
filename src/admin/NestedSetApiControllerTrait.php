<?php
namespace netfant\category\admin;

/**
 * Nested Set Api Controller Trait
 *
 * This trait should be added to the category api controller.
 *
 * ```
 * <?php
 * namespace app\modules\category\admin\api;
 *
 * class CategoryController extends \luya\admin\ngrest\base\Api
 * {
 *      use NestedSetApiControllerTrait;
 *
 *      @var string The path to the model which is the provider for the rules and fields.
 *      public $modelClass = 'app\modules\category\models\Category'
 * }```
 *
 * @author    Alexander Schmid <schmid@netfant.ch>
 * @copyright 2019 NetFant Schmid
 * @version   1.0.0
 * @since     1.0.0
 */
trait NestedSetApiControllerTrait
{
    public function actions()
    {
        $actions = parent::actions();

        $actions['create'] = [
            'class' => 'netfant\category\admin\ngrest\nestedset\actions\CreateAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
            'scenario' => $this->createScenario,
        ];
        $actions['update'] = [
            'class' => 'netfant\category\admin\ngrest\nestedset\actions\UpdateAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
            'scenario' => $this->updateScenario,
        ];
        $actions['delete'] = [
            'class' => 'netfant\category\admin\ngrest\nestedset\actions\DeleteAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        return $actions;
    }
}