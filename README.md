# LUYA CATEGORY EXTENSION

[![NetFant](https://img.shields.io/badge/powered_by-netfant-orange.svg?longCache=true&style=flat-square)](https://www.netfant.ch)
[![Packagist](https://img.shields.io/packagist/dt/netfant/luya-category.svg?longCache=true&style=flat-square)](https://gitlab.com/NetFant/LUYA/Extensions/Category/)
[![Packagist](https://img.shields.io/packagist/l/netfant/luya-category.svg?longCache=true&style=flat-square)](https://gitlab.com/NetFant/LUYA/Extensions/Category/blob/master/LICENSE)
[![Packagist](https://img.shields.io/packagist/v/netfant/luya-category.svg?longCache=true&style=flat-square)](https://packagist.org/packages/netfant/luya-category)

This package uses [Yii2 Nested Sets](https://github.com/creocoder/yii2-nested-sets) for easy nested sets crud actions.

## Features

- Can easily be added to models
- CRUD-Actions with nested sets.

## Installation & Configuration

### Composer installation

```
composer require netfant/luya-category:dev-master
```

### Configuration

Adjust the model to be similar to this:

```
namespace app\modules\category\models;

use Yii;
use luya\admin\ngrest\base\NgRestModel;

/**
 * Category Model
 */
class Category extends NgRestModel
{
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['restcreate'][] = 'operation';
        $scenarios['restcreate'][] = 'operationItem';
        $scenarios['restupdate'][] = 'operation';
        $scenarios['restupdate'][] = 'operationItem';
        return $scenarios;
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'tree' => [
                'class' => NestedSetModelBehavior::class
            ]
        ]);
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lft' => Yii::t('app', 'Lft'),
            'rgt' => Yii::t('app', 'Rgt'),
            'depth' => Yii::t('app', 'Depth'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'operation' => Yii::t('app', 'Operation'),
            'operationItem' => Yii::t('app', 'Item'),
        ];
    }
    
    public function ngRestAttributeTypes()
    {
        return [
            'lft' => 'number',
            'rgt' => 'number',
            'depth' => 'number',
            'name' => 'text',
            'slug' => ['slug', 'listener' => 'name'],
        ];
    }

    public function ngRestScopes()
    {
        return [
            ['list', ['lft', 'rgt', 'depth', 'name', 'slug']],
            [['create', 'update'], ['name', 'operation', 'operationItem', 'slug']],
            ['delete', true],
        ];
    }

    public function extraFields()
    {
        return [
            'operation',
            'operationItem'
        ];
    }

    public function ngrestExtraAttributeTypes()
    {
        return [
            'operation' => [
                'selectArray',
                'data' => [
                    NestedSetModelBehavior::OPERATION_MAKE_ROOT => 'Make root',
                    NestedSetModelBehavior::OPERATION_PREPEND_TO => 'Prepend to',
                    NestedSetModelBehavior::OPERATION_APPEND_TO => 'Append to',
                    NestedSetModelBehavior::OPERATION_INSERT_BEFORE => 'Insert before',
                    NestedSetModelBehavior::OPERATION_INSERT_AFTER => 'Insert after',
                ]
            ],
            'operationItem' => [
                'selectModel',
                'modelClass' => Category::class,
                'valueField' => 'id',
                'labelField' => 'name',
            ],

        ];
    }

    public function ngRestAttributeGroups()
    {
        return [
            [['operation', 'operationItem'], 'Category', 'collapsed' => false],
        ];
    }
}
```

Adjust the API controller to use the trait

```
namespace app\modules\category\admin\apis;

/**
 * Category API Controller.
 * 
 * File has been created with `crud/create` command. 
 */
class CategoryController extends \luya\admin\ngrest\base\Api
{
    use NestedSetApiControllerTrait;

    /**
     * @var string The path to the model which is the provider for the rules and fields.
     */
    public $modelClass = 'app\modules\category\models\Category';
}
```